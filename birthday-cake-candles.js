// 'use strict';

// const fs = require('fs');

// process.stdin.resume();
// process.stdin.setEncoding('utf-8');

// let inputString = '';
// let currentLine = 0;

// process.stdin.on('data', function(inputStdin) {
//     inputString += inputStdin;
// });

// process.stdin.on('end', function() {
//     inputString = inputString.split('\n');

//     main();
// });

// function readLine() {
//     return inputString[currentLine++];
// }

// /*
//  * Complete the 'birthdayCakeCandles' function below.
//  *
//  * The function is expected to return an INTEGER.
//  * The function accepts INTEGER_ARRAY candles as parameter.
//  */

function birthdayCakeCandles(candles) {
    // Write your code here
    // deklarasi variabel sebagai tempat menampung angka tertinggi dari array
    var maxHeight = Math.max(...candles);
    // variabel untuk menampung hasil perhitumgan jumlah angka tertinggi
    var maxHeightCount = 0;  
    // perulangan untuk mengecek angka 1 per 1
    for(var i = 0; i < candles.length; i++){
        // jika dalam array terdapat angka yang sama dengan angka yang terdapat pada variabel maxHeight
        if (candles[i] == maxHeight){
            // maka variabel maxHeightCount akan bertambah 1, 
            // Sehingga dapat menghitung jumlah angka tertinggi yg ada
            maxHeightCount++ 
        }
    }
    console.log(maxHeightCount);
    return maxHeightCount;
}

arr =[3,2,1,3]
birthdayCakeCandles(arr);

// function main() {
//     const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

//     const candlesCount = parseInt(readLine().trim(), 10);

//     const candles = readLine().replace(/\s+$/g, '').split(' ').map(candlesTemp => parseInt(candlesTemp, 10));

//     const result = birthdayCakeCandles(candles);

//     ws.write(result + '\n');

//     ws.end();
// }
