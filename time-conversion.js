// 'use strict';

// const fs = require('fs');

// process.stdin.resume();
// process.stdin.setEncoding('utf-8');

// let inputString = '';
// let currentLine = 0;

// process.stdin.on('data', function(inputStdin) {
//     inputString += inputStdin;
// });

// process.stdin.on('end', function() {
//     inputString = inputString.split('\n');

//     main();
// });

// function readLine() {
//     return inputString[currentLine++];
// }

// /*
//  * Complete the 'timeConversion' function below.
//  *
//  * The function is expected to return a STRING.
//  * The function accepts STRING s as parameter.
//  */

function timeConversion(s) {
    // Write your code here
    // menggunakan split method untuk memisahkan jam , menit , dan detik
    const [hours, minutes, remaining] = s.split(':');
    // karna dalam remaining angka detik dan Huruf AM/PM masih menyatu
    // maka dipisahkan dengan menggunakan method substr 
    // di ambil dari string ke 0 sepanjang 2 huruf
    const secs = remaining.substr(0,2);
    // mengambil string ke 2 yaitu huruf AM/PM
    const time = remaining.substr(2);
    // deklarasi variabel yang menampung hasil
    let result = ''
    // kondisi untuk membuat jam 12 malam menjadi 00
    if (time === 'AM' && hours == 12){
        // jam di rubah menjadi 00, kemudian di satukan menjadi string 
        // menggunakan method join dengan dipisahkan karakter (:)
        result = ["00", minutes, secs].join(":")
    // kodisi untuk merubah format 12 jam menjadi 24jam
    }else if (time === 'PM' && hours < 12) {
        // jam di rubah menjadi integer kemudian di tambah 12
        let hour = parseInt(hours) + 12;
        // kemudian di satukan kembali menjadi 1 string
        // menggunakan method join dengan dipisahkan karakter (:)
        result = [hour, minutes, secs].join(":");
    } 
    return (result)
}
// sample test
const timeStrPm = '07:05:45PM';
const timeStrAm = '12:01:30AM';
console.log(timeConversion(timeStrPm));
console.log(timeConversion(timeStrAm));

// function main() {
//     const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

//     const s = readLine();

//     const result = timeConversion(s);

//     ws.write(result + '\n');

//     ws.end();
// }
