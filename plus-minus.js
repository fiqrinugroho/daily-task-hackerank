// 'use strict';

// process.stdin.resume();
// process.stdin.setEncoding('utf-8');

// let inputString = '';
// let currentLine = 0;

// process.stdin.on('data', function(inputStdin) {
//     inputString += inputStdin;
// });

// process.stdin.on('end', function() {
//     inputString = inputString.split('\n');

//     main();
// });

// function readLine() {
//     return inputString[currentLine++];
// }

// /*
//  * Complete the 'plusMinus' function below.
//  *
//  * The function accepts INTEGER_ARRAY arr as parameter.
//  */

function plusMinus(arr) {
    // Write your code here
    // deklarasi variabel dengan niali 0
    let positives = 0, negatives = 0, zeroes = 0;
    // looping sepanjang array
     for (let i = 0; i < arr.length; i++) {
        // jika lebih besar dari nol maka positiv
        if (arr[i] > 0){ 
            // Tambah 1 ke variabel positives
            positives++
        // jika sama dengan 0  
        } else if (arr[i] === 0) {
            // Tambah 1 ke variabel zeroes
            zeroes++
        // jika tidak masuk keduanya maka masuk ke variabel negatives
        }else { 
            // tambah 1 ke variabel negatives
            negatives++
        }
    }
    // deklarasi variabel total yang menyimpan panjangnya array
    let total = arr.length;
    // membagi variabel dengan panjang array
    let positive = positives/total
    let negative = negatives/total
    let zero = zeroes/total
    console.log(positive);
    console.log(negative);
    console.log(zero);
    return(positive,negative,zero)
}

arr = [-4, 3, -9, 0, 4, 1]
plusMinus(arr);

// function main() {
//     const n = parseInt(readLine().trim(), 10);

//     const arr = readLine().replace(/\s+$/g, '').split(' ').map(arrTemp => parseInt(arrTemp, 10));

//     plusMinus(arr);
// }
