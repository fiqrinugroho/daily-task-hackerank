// 'use strict';

// process.stdin.resume();
// process.stdin.setEncoding('utf-8');

// let inputString = '';
// let currentLine = 0;

// process.stdin.on('data', function(inputStdin) {
//     inputString += inputStdin;
// });

// process.stdin.on('end', function() {
//     inputString = inputString.split('\n');

//     main();
// });

// function readLine() {
//     return inputString[currentLine++];
// }

// /*
//  * Complete the 'miniMaxSum' function below.
//  *
//  * The function accepts INTEGER_ARRAY arr as parameter.
//  */

function miniMaxSum(arr) {
    // Write your code here
    // deklarasi varabel arrayNumber menjadi array kosong
    let arrayNumber= new Array(0);
    // loopig sepanjang array
    for (let i = 0; i < arr.length; i++) {
        // deklarasi variabel sumTotal didalam looping agar nialinya kembali 0
        let  sumTotal = 0; 
        for (let j = i + 1; j < arr.length; j++) { 
            sumTotal += arr[j]
        }
        for (let j = 0; j < i; j++) {
            sumTotal += arr[j]
        } 
        // memasukan angka ke arrayNumber
        arrayNumber.push(sumTotal)
        
    }
    // deklarasi variabel min dengan fungsi pencari nilai min pada array
    const min = Math.min(...arrayNumber)
     // deklarasi variabel max dengan fungsi pencari nilai max pada array
    const max = Math.max(...arrayNumber)
    console.log(min,max)
    return(min,max)

}

arr = [1,2,3,4,5]
miniMaxSum(arr);

// function main() {

//     const arr = readLine().replace(/\s+$/g, '').split(' ').map(arrTemp => parseInt(arrTemp, 10));

//     miniMaxSum(arr);
// }
