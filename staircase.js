// 'use strict';

// process.stdin.resume();
// process.stdin.setEncoding('utf-8');

// let inputString = '';
// let currentLine = 0;

// process.stdin.on('data', function(inputStdin) {
//     inputString += inputStdin;
// });

// process.stdin.on('end', function() {
//     inputString = inputString.split('\n');

//     main();
// });

// function readLine() {
//     return inputString[currentLine++];
// }

/*
 * Complete the 'staircase' function below.
 *
 * The function accepts INTEGER n as parameter.
 */

function staircase(n) {
    // Write your code here
    // deklarasi variabel row sebagai string kosong
    let row = "";
    // looping sebanyak variabel n
    for(let i = 0; i < n; i++){
        // looping sebanyak variabel n-1
        for(let j = i; j < n - 1; j++){
            // setiap putaran menambahkan spasi sebanyak n - 1
            row += " ";
        }
        // looping sebanyak variabel i + 1
        for(let j = 0; j < i + 1 ; j++){
            // memcetak # sesuai dengan variabel i saat ini
            row += "#";
        }
        // mencetak variabel row
        console.log(row);
        // mengembalikan variabel row menjadi kosong
        row = "";
    }
}

// memanggil fungsi
staircase(6);

// function main() {
//     const n = parseInt(readLine().trim(), 10);
    
//     staircase(n);
// }

